#ifndef _BATTERY_H
#define _BATTERY_H

#include <stdint.h>

extern void battery_init(void);
extern void battery_uninit(void);
extern int32_t battery_calcV(int32_t adc);
extern int32_t battery_readAdc();

#endif
