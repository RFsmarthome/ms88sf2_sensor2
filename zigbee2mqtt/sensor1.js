const fz = require('zigbee-herdsman-converters/converters/fromZigbee');
const tz = require('zigbee-herdsman-converters/converters/toZigbee');
const exposes = require('zigbee-herdsman-converters/lib/exposes');
const reporting = require('zigbee-herdsman-converters/lib/reporting');
const ota = require('zigbee-herdsman-converters/lib/ota');
const e = exposes.presets;

const schnakeConverters = {
    pressure: {
        cluster: 'msPressureMeasurement',
        type: ['attributeReport', 'readResponse'],
        convert: (model, msg, publish, options, meta) => {
            let pressure = 0;
            pressure = parseFloat(msg.data['measuredValue']) / 10.0;
            return {pressure: pressure};
        },
    }
};

const deviceSensor1 = {
    zigbeeModel: ['sensor1'],
    model: 'sensor1',
    vendor: 'Schnake',
    description: 'Schnakes Sensor type 1',
    fromZigbee: [fz.temperature, schnakeConverters.pressure, fz.humidity, fz.battery],
    toZigbee: [],
    exposes: [e.temperature(), e.humidity(), e.pressure(), e.battery(), e.voltage()],
    ota: ota.zigbeeOTA,
    meta: {configureKey: 1},
    configure: async (device, coordinatorEndpoint, logger) => {
        const endpoint = device.getEndpoint(10);
        await reporting.bind(
            endpoint,
            coordinatorEndpoint,
            ['genPowerCfg', 'msTemperatureMeasurement', 'msRelativeHumidity', 'msPressureMeasurement']);

        
        await reporting.temperature(endpoint, {min: 0, max: 60, change: 0});
        await reporting.pressure(endpoint, {min: 0, max: 60, change: 0});
        await reporting.humidity(endpoint, {min: 0, max: 60, change: 0});

        await reporting.batteryPercentageRemaining(endpoint, {min: 300, max: 900, change: 0});
        await reporting.batteryVoltage(endpoint, {min: 300, max: 900, change: 0});
    }
};

module.exports = deviceSensor1;
