const fz = require('zigbee-herdsman-converters/converters/fromZigbee');
const tz = require('zigbee-herdsman-converters/converters/toZigbee');
const exposes = require('zigbee-herdsman-converters/lib/exposes');
const reporting = require('zigbee-herdsman-converters/lib/reporting');
const ota = require('zigbee-herdsman-converters/lib/ota');
const e = exposes.presets;

const deviceSensor2 = {
    zigbeeModel: ['sensor2'],
    model: 'sensor2',
    vendor: 'Schnake',
    description: 'Schnakes Sensor type 2',
    fromZigbee: [fz.temperature, fz.humidity, fz.battery],
    toZigbee: [],
    exposes: [e.temperature(), e.humidity(), e.battery(), e.voltage()],
    ota: ota.zigbeeOTA,
    meta: {configureKey: 1},
    configure: async (device, coordinatorEndpoint, logger) => {
        const endpoint = device.getEndpoint(10);
        await reporting.bind(
            endpoint,
            coordinatorEndpoint,
            ['genPowerCfg', 'msTemperatureMeasurement', 'msRelativeHumidity']);

        
        await reporting.temperature(endpoint, {min: 0, max: 60, change: 0});
        await reporting.humidity(endpoint, {min: 0, max: 60, change: 0});

        await reporting.batteryPercentageRemaining(endpoint, {min: 300, max: 900, change: 0});
        await reporting.batteryVoltage(endpoint, {min: 300, max: 900, change: 0});
    }
};

module.exports = deviceSensor2;
